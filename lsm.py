import sys
import pygame
import math
import random
import colorsys

class Node:
    def __init__(self, size):
        self.size = size
        self.fill = 0

    def insert(self, n):
        # check if there is room
        if self.fill + n < self.size:
            self.fill = self.fill + n
        return (self.size - self.fill) - n

class LSM:
    def __init__(self, m, node_size, screen):
        self.m = m
        self.node_size = node_size
        self.screen = screen
        self.font = pygame.font.Font(None, 18)
        self.merge_counter = 0
        self.levels = [] # Track number of nodes in each level
        self.thresh = []
        self.merge_set = set()
        self.inserts = 0
        self.reads = 0
        self.writes = 0

        # Create in-memory node
        mem = Node(node_size)

        # Level 0 info
        self.add_new_level(0)

    def insert(self):
        self.inserts = self.inserts + 1
        self.writes = self.writes + 1
        self.levels[0] = self.levels[0] + 1
        if self.levels[0] > self.thresh[0]:
            self.merge(0)

    def add_new_level(self, level):
        self.levels.append(0)
        if level == 0:
            self.thresh.append(4)
        else:
            self.thresh.append(self.m ** level)

    def find_overlapping(self, level, index):
        if level == 0:
            return [(1, i) for i in range(self.levels[1])]
        else:
            l = []
            num = self.levels[level + 1] / self.levels[level]
            for i in range(num):
                l.append((level + 1, index * num + i))
            return l

    def merge(self, level):
        self.update_image()

        if len(self.levels) == level + 1:
            self.add_new_level(level + 1)

        index = random.randrange(0, self.levels[level])
        # Add nodes invovled in merge to set to colorize
        # First select nodes in 'level' that are to be merged
        if level == 0:
            for l0 in [(0, i) for i in range(self.levels[0])]:
                self.merge_set.add(l0)
        else:
            self.merge_set.add((level, index))
        # then look for overlapping nodes in level+1
        olap = self.find_overlapping(level, index)
        for c in olap:
            self.merge_set.add(c)
        self.update_image()
        # remove from 'level'
        if level == 0:
            for l0 in [(0, i) for i in range(self.levels[0])]:
                self.merge_set.remove(l0)
        else:
            self.merge_set.remove((level, index))
        # remove from 'level+1'
        for c in olap:
            self.merge_set.remove(c)

        if level == 0:
            self.reads = self.reads + self.levels[0] + self.levels[1]
            self.writes = self.writes + self.levels[0] + self.levels[1]
            self.levels[1] = self.levels[1] + self.levels[0]
            self.levels[0] = 0
        else:
            self.reads = self.reads + 1 + len(olap)
            self.writes = self.writes + 1 + len(olap)
            self.levels[level] = self.levels[level] - 1
            self.levels[level + 1] = self.levels[level + 1] + 1
            if self.levels[level] > self.thresh[level]:
                self.merge(level)

        if self.levels[level + 1] > self.thresh[level + 1]:
            self.merge(level + 1)

    def draw_node(self, level, index):
        blue = 0, 0, 255
        red = 255, 0, 0
        midx = 512
        midy = 120
        nwidth = 2
        nheight = 20
        xgap = 2
        ygap = 10

        if (level, index) in self.merge_set:
            col = red
        else:
            if level == 0:
                col = blue
            else:
                col = (100 + 155 * float(index) / self.levels[level], 100 + 155 * float(index) / self.levels[level], 255)
        nod = pygame.Rect(100 + index * (xgap + nwidth), midy + level * (ygap + nheight), nwidth, nheight)
        pygame.draw.rect(self.screen, col, nod, 2)
        
    def update_image(self):
        white = 255, 255, 255
        self.screen.fill(white)

        midx = 512
        midy = 120
        nheight = 20
        ygap = 10

        text = self.font.render("Inserts: " + str(self.inserts * 32768), 1, (10, 10, 10))
        textpos = text.get_rect(centerx = 100, centery = 50)
        self.screen.blit(text, textpos)

        text = self.font.render("Total reads: " + str(self.reads * (self.node_size >> 14)) + " MBs", 1, (10, 10, 10))
        textpos = text.get_rect(centerx = 100, centery = 70)
        self.screen.blit(text, textpos)

        text = self.font.render("Total writes: " + str(self.writes * (self.node_size >> 14)) + " MBs", 1, (10, 10, 10))
        textpos = text.get_rect(centerx = 100, centery = 90)
        self.screen.blit(text, textpos)

        for l in range(len(self.levels)):
            text = self.font.render("Level " + str(l), 1, (10, 10, 10))
            textpos = text.get_rect(centerx = 50, centery = midy + l * (ygap + nheight) + nheight / 2)
            self.screen.blit(text, textpos)
            for i in range(self.levels[l]):
                self.draw_node(l, i)

        pygame.display.flip()
        pygame.time.delay(1000)


def main():
    pygame.init()
    pygame.display.set_caption('LSM Tree')
    pygame.mouse.set_visible(1)

    node_size = 32768
    inserted = 0
    total = 10000000
    screen = pygame.display.set_mode((1024, 768))

    lsm = LSM(10, node_size, screen)

    while inserted < total:
        inserted = inserted + node_size
        lsm.insert()
        lsm.update_image()

    while (pygame.event.wait().type != pygame.KEYDOWN): pass

if __name__ == "__main__":
    main()

