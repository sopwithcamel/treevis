import sys
import pygame
import math
import random
import colorsys

class Node:
    def __init__(self, id):
        self.id = id
        self.cap = 16
        self.size = []
        self.children = [] # Tracks ids of children

class BufferTree:
    def __init__(self, f, node_size, screen):
        self.fanout = f
        self.node_size = node_size
        self.screen = screen
        self.font = pygame.font.Font(None, 18)
        self.nodectr = 0
        self.nodemap = dict()
        self.parentmap = dict()

        self.root = self.get_new_node()
        self.root.size.append(0)
        self.parentmap[self.root.id] = -1

        self.merge_set = set()
        self.inserts = 0
        self.reads = 0
        self.writes = 0

    ''' To be used to create new nodes '''
    def get_new_node(self):
        nod = Node(self.nodectr)
        self.nodemap[self.nodectr] = nod
        self.nodectr = self.nodectr + 1
        return nod        

    def insert(self):
        self.inserts = self.inserts + 1
#        self.writes = self.writes + 1
        if (len(self.root.size) == 0):
            self.root.size.append(0)
        self.root.size[0] = self.root.size[0] + 1
        if sum(self.root.size) > self.root.cap:
            self.spill(self.root.id)
        self.update_image()

    def split_leaf(self, id):
        new_leaf = self.get_new_node() 
        nod = self.nodemap[id]
        tot_size = sum(nod.size)
        new_leaf.size.append(tot_size / 2)
        nod.size = [tot_size - tot_size / 2]

        par_id = self.parentmap[nod.id] 

        # Create new root if necessary
        if par_id == -1:
            new_root = self.get_new_node()
            par_id = new_root.id
            self.parentmap[new_root.id] = -1
            self.root = new_root
            new_root.children.append(nod.id)

        # Set parents
        self.parentmap[nod.id] = par_id
        self.parentmap[new_leaf.id] = par_id

        # Add children to parent
        parent = self.nodemap[par_id]
        parent.children.append(new_leaf.id)

        self.writes = self.writes + tot_size
        self.reads = self.writes + tot_size

    def spilt_non_leaf(self, id):
        new_leaf = self.get_new_node() 
        nod = self.nodemap[id]
        num_c = len(nod.children)
        new_leaf.children.extend(nod.children[num_c / 2:])

        # Change parent of all children being transferred
        for c in new_leaf.children:
            self.parentmap[c] = new_leaf.id
        nod.children = nod.children[:num_c/2]

        par_id = self.parentmap[nod.id] 
        # Create new root if necessary
        if par_id == -1:
            new_root = self.get_new_node()
            par_id = new_root.id
            self.parentmap[new_root.id] = -1
            self.root = new_root

        # Set parents
        self.parentmap[nod.id] = par_id
        self.parentmap[new_leaf.id] = par_id

        # Add children to parent
        parent = self.nodemap[par_id]
        parent.children.append(nod.id)
        parent.children.append(new_leaf.id)

    def spill(self, id):
        self.merge_set.add(id)
        nod = self.nodemap[id]
        self.update_image()
        self.merge_set.remove(id)
        # If it is a leaf we need to split
        if len(nod.children) == 0:
            self.split_leaf(id)
        else:
            rem_size = sum(nod.size)
            self.writes = self.writes + rem_size
            num_c = len(nod.children)
            for c in nod.children:
                s = rem_size / num_c
                self.nodemap[c].size.append(s)
                rem_size = rem_size - s
                num_c = num_c - 1
            nod.size = []
            for c in nod.children:
                if sum(self.nodemap[c].size) >= self.nodemap[c].cap:
                    self.spill(c)
            if len(nod.children) > self.fanout:
                self.spilt_non_leaf(id)
        self.update_image()

    ''' Draw node nod at level level at index index / tot '''
    def draw_node(self, nod, level, index, tot):
        blue = 0, 0, 255
        red = 255, 0, 0
        midx = 512
        midy = 384
        nwidth = 10
        nheight = 2
        xgap = 5
        ygap = 2
        levelygap = nod.cap * nheight + 4 * ygap

        if nod.id in self.merge_set:
            col = red
        else:
            col = blue
        startx = midx + (index - tot / 2) * 12
        starty = midy + level * (levelygap)
        size_ctr = 0
        for s in nod.size:
            for i in range(s):
                if col != red and nod.id == self.root.id:
                    col = blue
                elif col != red:
                    ifl = float(i)
                    col = (100 + 155 * ifl / s, 100 + 155 * ifl / s, 255)
                r = pygame.Rect(startx, starty + size_ctr + i * nheight, nwidth, nheight)
                pygame.draw.rect(self.screen, col, r, 0)
            size_ctr = size_ctr + s * nheight + ygap
        
    def update_image(self):
        white = 255, 255, 255
        self.screen.fill(white)

        midx = 512
        midy = 384
        nheight = 20
        ygap = 10

        text = self.font.render("Inserts: " + str(self.inserts * 32768), 1, (10, 10, 10))
        textpos = text.get_rect(centerx = 100, centery = 200)
        self.screen.blit(text, textpos)

        text = self.font.render("Total reads: " + str(self.reads * (32768 >> 14)) + " MBs", 1, (10, 10, 10))
        textpos = text.get_rect(centerx = 100, centery = 220)
        self.screen.blit(text, textpos)

        text = self.font.render("Total writes: " + str(self.writes * (32768 >> 14)) + " MBs", 1, (10, 10, 10))
        textpos = text.get_rect(centerx = 100, centery = 240)
        self.screen.blit(text, textpos)

        first_list = []
        second_list = []
        first_list.append(self.root.id)
        level = 0
        while True:
            ctr = 0
            for id in first_list:
                nod = self.nodemap[id]
                self.draw_node(nod, level, ctr, len(first_list))
                ctr = ctr + 1
                second_list.extend(nod.children)
            level = level + 1
            if len(second_list) == 0:
                break
            first_list = second_list
            second_list = []
#            print "First list: ", first_list
#            print "Second list: ", second_list

        pygame.display.flip()
        pygame.time.delay(200)


def main():
    pygame.init()
    pygame.display.set_caption('Buffer Tree')
    pygame.mouse.set_visible(0)

    node_size = 491520 # 30MB
    inserted = 0
    total = 10000000
    screen = pygame.display.set_mode((1024, 768))

    tree = BufferTree(16, node_size, screen)

    while inserted < total:
        inserted = inserted + 32768
        tree.insert()
        tree.update_image()

    while (pygame.event.wait().type != pygame.KEYDOWN): pass

if __name__ == "__main__":
    main()

